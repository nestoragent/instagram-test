package com.instagram.lib;

import com.instagram.lib.pageFactory.PageFactory;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.MessageFormat;

/**
 * Created by VelichkoAA on 12.01.2016.
 */
public class Init {
    protected static WebDriver driver;
    private static PageFactory pageFactory;

    public static WebDriver getDriver() {
        if (null == driver) {
            String sauceUserName = "";
            String sauceAccessKey = "";

            DesiredCapabilities caps = DesiredCapabilities.android();
            caps.setCapability("appiumVersion", "1.4.16");
            caps.setCapability("deviceName","Samsung Galaxy Nexus Emulator");
            caps.setCapability("deviceOrientation", "portrait");
            caps.setCapability("browserName", "");
            caps.setCapability("platformVersion","4.4");
            caps.setCapability("platformName","Android");
//            caps.setCapability("app","sauce-storage:gapps.zip");
            caps.setCapability("app","sauce-storage:Instagram.apk");
            caps.setJavascriptEnabled(true);

            //old
//            String path = System.getProperty("user.dir");
//            File app = new File(path, "\\apps\\instagram.apk");
//            DesiredCapabilities capabilities = new DesiredCapabilities();
//            capabilities.setCapability("device", "Android");

            //mandatory capabilities
//            capabilities.setCapability("deviceName", "Android");
//            capabilities.setCapability("platformName", "Android");

            //other caps
//            capabilities.setCapability("newCommandTimeout", "180");
//            capabilities.setCapability("appPackage", "com.instagram.android");
//            capabilities.setCapability("appActivity", ".nux.SignedOutFragmentActivity");
//            capabilities.setCapability("appActivity", "com.instagram.android.activity.MainTabActivity");
            caps.setCapability("appWaitActivity", "com.instagram.android.activity.MainTabActivity");
//            capabilities.setCapability("app", app.getAbsolutePath());

            try {
                driver = new RemoteWebDriver(new URL(MessageFormat.format("http://{0}:{1}@ondemand.saucelabs.com:80/wd/hub", sauceUserName, sauceAccessKey)),
                        caps);
            } catch (MalformedURLException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
        return driver;
    }

    public static PageFactory getPageFactory() {
        if (null == pageFactory) {
            pageFactory = new PageFactory("com.instagram.pages");
        }
        return pageFactory;
    }
}
