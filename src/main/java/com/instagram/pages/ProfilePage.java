package com.instagram.pages;

import com.instagram.lib.Init;
import com.instagram.lib.Page;
import com.instagram.lib.pageFactory.PageEntry;
import org.openqa.selenium.By;

/**
 * Created by VelichkoAA on 12.01.2016.
 */
@PageEntry(title = "Profile")
public class ProfilePage extends Page {

    public ProfilePage() {
        waitForVisibilityOf(By.xpath("//android.widget.FrameLayout[@content-desc='Profile']/android.widget.ImageView"));
    }

    public void logOut () {
        Init.getDriver().findElement(By.xpath("//android.widget.ImageView[@content-desc='Options']")).click();
        waitForVisibilityOf(By.xpath("//android.widget.TextView[@text='Options']"));
//        scrollPageDown();
        Init.getDriver().findElement(By.xpath("//android.widget.TextView[@text='Log Out']")).click();
        waitForVisibilityOf(By.id("com.instagram.android:id/button_positive"));
        Init.getDriver().findElement(By.id("com.instagram.android:id/button_positive")).click();
    }

}
