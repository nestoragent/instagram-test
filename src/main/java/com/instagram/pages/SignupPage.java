package com.instagram.pages;

import com.instagram.lib.Init;
import com.instagram.lib.Page;
import org.openqa.selenium.By;

/**
 * Created by VelichkoAA on 12.01.2016.
 */
public class SignupPage extends Page {

    public SignupPage(){
        waitForVisibilityOf(By.id("com.instagram.android:id/log_in"));
    }

    public void pressButtonLogin() {
        Init.getDriver().findElement(By.id("com.instagram.android:id/log_in")).click();
    }

}
