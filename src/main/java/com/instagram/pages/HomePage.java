package com.instagram.pages;

import com.instagram.lib.Init;
import com.instagram.lib.Page;
import com.instagram.lib.pageFactory.PageEntry;
import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;

import javax.swing.*;

/**
 * Created by VelichkoAA on 12.01.2016.
 */
@PageEntry(title = "Home")
public class HomePage extends Page {

    public HomePage() {
        waitForVisibilityOf(By.xpath("//android.widget.FrameLayout[@content-desc='Home']/android.widget.ImageView"));
    }

    public void clickOnButtonCamera () {
        Init.getDriver().findElement(By.xpath("//android.widget.FrameLayout[@content-desc='Camera']/android.widget.ImageView")).click();
    }

    public void clickOnButtonProfile () {
        Init.getDriver().findElement(By.xpath("//android.widget.FrameLayout[@content-desc='Profile']/android.widget.ImageView")).click();
    }

}
