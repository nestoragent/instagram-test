package com.instagram.pages;

import com.instagram.lib.Init;
import com.instagram.lib.Page;
import com.instagram.lib.pageFactory.PageEntry;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

/**
 * Created by VelichkoAA on 12.01.2016.
 */
@PageEntry(title = "Login")
public class LoginPage extends Page {

    public LoginPage() {
        waitForVisibilityOf(By.id("com.instagram.android:id/login_username"));
    }

    public void login(String login, String password) {
        List<WebElement> loginButton = Init.getDriver().findElements(By.id("com.instagram.android:id/log_in"));
        if (loginButton.size() > 0)
            loginButton.get(0).click();
        Init.getDriver().findElement(By.id("com.instagram.android:id/login_username")).sendKeys(login);
        Init.getDriver().findElement(By.id("com.instagram.android:id/login_password")).sendKeys(password);
        Init.getDriver().findElement(By.xpath("//android.widget.TextView[contains(@text, 'Log In')]")).click();
        try {
            Thread.sleep(10 * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void login() {
        String login = "myrtismoris5lan";
        String password = "gF8LMRLLS";

        List<WebElement> loginButton = Init.getDriver().findElements(By.id("com.instagram.android:id/log_in"));
        if (loginButton.size() > 0)
            loginButton.get(0).click();

        Init.getDriver().findElement(By.id("com.instagram.android:id/login_username")).sendKeys(login);
        Init.getDriver().findElement(By.id("com.instagram.android:id/login_password")).sendKeys(password);
        Init.getDriver().findElement(By.xpath("//android.widget.TextView[contains(@text, 'Log In')]")).click();
        try {
            Thread.sleep(10 * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
