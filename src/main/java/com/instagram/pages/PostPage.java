package com.instagram.pages;

import com.instagram.lib.Init;
import com.instagram.lib.Page;
import com.instagram.lib.pageFactory.PageEntry;
import org.openqa.selenium.By;

/**
 * Created by VelichkoAA on 14.01.2016.
 */
@PageEntry(title = "Message")
public class PostPage extends Page {

    public PostPage() {
        waitForVisibilityOf(By.xpath("//android.widget.TextView[@text='Photo']"));
    }

    public void postMessage () {
        Init.getDriver().findElement(By.xpath("//android.widget.TextView[@text='Photo']")).click();
        waitForVisibilityOf(By.id("com.instagram.android:id/shutter_button"));
        Init.getDriver().findElement(By.id("com.instagram.android:id/shutter_button")).click();
        waitForVisibilityOf(By.xpath("//android.widget.TextView[@text='Edit']"));
        Init.getDriver().findElement(By.id("com.instagram.android:id/button_next")).click();
        waitForVisibilityOf(By.id("com.instagram.android:id/caption_text_view"));
        Init.getDriver().findElement(By.id("com.instagram.android:id/caption_text_view")).sendKeys("Some text");
        Init.getDriver().findElement(By.id("com.instagram.android:id/button_next")).click();

        closeNotificationsRate();
    }


}
