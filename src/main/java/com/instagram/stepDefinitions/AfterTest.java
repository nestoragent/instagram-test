package com.instagram.stepDefinitions;

import com.instagram.pages.HomePage;
import com.instagram.pages.ProfilePage;
import cucumber.api.java.After;

/**
 * Created by VelichkoAA on 14.01.2016.
 */
public class AfterTest {

    @After
    public void logOut() {
        new HomePage().clickOnButtonProfile();
        new ProfilePage().logOut();
    }
}
